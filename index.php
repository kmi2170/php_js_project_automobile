<?php
require_once "pdo.php";

session_start();
echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            #success {
                color: green;
            }
            #error {
                color: red;
            }
            th, td  {
                border: 1px solid black;
                border-color: grey;
            }
        </style>
    </head>
    <body>
        <h1>Welcome to the Automobiles Database</h1>

        <?php
        if (isset($_SESSION['success'])) {
            echo('<p id="success">'.$_SESSION['success']."</p>\n");
            unset($_SESSION['success']);
        }
        if (isset($_SESSION['error'])) {
            echo('<p id="error">'.$_SESSION['error']."</p>\n");
            unset($_SESSION['error']);
        }
        ?>

        <?php if (isset($_SESSION['name'])) { ?>

        <h3> Your user name: <?= htmlentities($_SESSION['name']) ?> </h3>
        <a href="add.php">Add New Entry</a>
        <a href="logout.php">Log Out</a>

        <table>
            <tr>
                <th>Year</th><th>Make</th>
                <th>Mileage</th><th>Action</th>
            </tr>
        <?php
        $stmt = $pdo->query("SELECT auto_id, make, year, mileage, url FROM autos ORDER BY make, year, mileage");

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            echo "<tr><td>";
            echo(htmlentities($row['year']));
            echo "</td><td>";
            echo(htmlentities($row['make']));
            if (strlen($row['url']) > 0) {
                echo(" <a href=".htmlentities($row['url'])." target='_blank'>PIC</a>");
            }
            echo "</td><td>";
            echo(htmlentities($row['mileage']));
            echo "</td><td>";
            echo('<a href="edit.php?id='.$row['auto_id'].'">Edit / </a>');
            echo('<a href="delete.php?id='.$row['auto_id'].'">Delete</a>');
            echo "</td></tr>\n";
        }
        ?>
        </table>

        <?php } else { ?>
        <a href="login.php">Please Log In</a>
        <?php } ?>

    </body>
</html>
