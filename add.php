<?php
require_once "pdo.php";

session_start();
echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);

if (!isset($_SESSION['name'])) {
    die('Access Denied');
}

if (isset($_POST['clear'])) {
    header("Location: add.php");
    return;
}

if (isset($_POST['make']) && isset($_POST['year']) && isset($_POST['mileage'])) {
    if (! empty($_POST['make']) && ! empty($_POST['year']) && ! empty($_POST['mileage'])) {
        if (! is_numeric($_POST['year'])) {
            $_SESSION['error'] = "Year must be numeric";
            header("Location: add.php");
            return;
        }
        if (! is_numeric($_POST['mileage'])) {
            $_SESSION['error'] = "Mileage must be numeric";
            header("Location: add.php");
            return;
        }

        if (! empty($_POST['url'])) {
//    if (filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
            $http_status = url_exists($_POST['url']);
            if ($http_status == 403) {
                $_SESSION['error'] = "URL doesn't exits";
                header("Location: add.php");
                return;
            }
            $value_url = $_POST['url'];
        } else {
            $value_url = NULL;
        }

        $sql = 'INSERT INTO autos (make, year, mileage, url) VALUES ( :mk, :yr, :mi, :ul)';
        echo("<p>".$sql."</p>");
        $stmt = $pdo->prepare($sql);
        $stmt->execute(
            array(
        ':mk' => $_POST['make'],
        ':yr' => $_POST['year'],
        ':mi' => $_POST['mileage'],
        ':ul' => $value_url)
        );

        /*
        if (! empty($_POST['url'])) {
            $sql = "SELECT auto_id FROM autos WHERE make=:mk AND year=:yr AND mileage=:mi";
            echo("<p>".$sql."</p>");
            $stmt = $pdo->prepare($sql);
            $stmt->execute(
                array(
            ':mk' => $_POST['make'],
            ':yr' => $_POST['year'],
            ':mi' => $_POST['mileage'])
            );
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $id = $row['auto_id'];

            $sql = 'UPDATE autos SET url= :ul WHERE auto_id='.$id;
            echo("<p>".$sql."</p>");
            $stmt = $pdo->prepare($sql);
            $stmt->execute(array(':ul' => $_POST['url']));
        }
        */

        $_SESSION['success'] = "Record Inserted";
        header("Location: index.php");
        return;
    } else {
        $_SESSION['error'] = "All fields are required";
        header("Location: add.php");
        return;
    }
}


function url_exists($url)
{
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch);

    $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    return $http_status;
}


?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title></title>
        <style media="screen">
            html {
                font-family: arial;
            }
            #msg{
                color: red;
            }
        </style>
    </head>
    <body>
        <h1>Add New Entry</h1>
        <h3> Your User Name: <?= htmlentities($_SESSION['name']) ?></h3>

        <?php
          if (isset($_SESSION['error'])) {
              echo("<p id='msg'>".$_SESSION['error']."</p>");
              unset($_SESSION['error']);
          }
        ?>

        <form method="post">
            <p>Make: <input type="text" name="make"></p>
            <p>Year: <input type="text" name="year"></p>
            <p>Mileage: <input type="text" name="mileage"></p>
            <p>Url for Image (optional): <input type="url" name="url"></p>
            <input type="submit" value="Add">
            <input type="submit" name="clear" value="Clear">
        </form>
        <p><a href="index.php">Cancel</a></p>


    </body>
</html>
