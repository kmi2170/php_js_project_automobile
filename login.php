<?php
session_start();

echo " SESSION ", var_dump($_SESSION);
echo "<br>";
echo " POST ", var_dump($_POST);
echo "<br>";
//var_dump( (! empty($_POST['name']) && ! empty($_POST['pwd'])) );
//var_dump( (isset($_POST['name']) && isset($_POST['pwd'])) );

if (isset($_POST['name']) && isset($_POST['pwd'])) {
    unset($_SESSION['name']);

    if (! empty($_POST['name']) && ! empty($_POST['pwd'])) {
//    if (strlen($_SESSION['name']) > 0 && strlen($_SESSION['pwd'] > 0)) {
        if (! filter_var($_POST['name'], FILTER_VALIDATE_EMAIL)) {
            $_SESSION['error'] = "Not a valid email address";
            header("Location: login.php");
            return;
        } else {
            $salt = 'XyZzy12*_';
            $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';
            // password: php123
            $md5 = hash('md5', $salt.$_POST['pwd']);

            if ($md5 == $stored_hash) {
                $_SESSION['name'] = $_POST['name'];
                $_SESSION['pwd'] = $_POST['pwd'];
                header("Location: index.php");
                return;
            } else {
                //         error_log("Login fail ".$_POST['name']);
                $_SESSION['error'] = "Incorrect password";
                header("Location: login.php");
                return;
            }
        }
    } else {
        $_SESSION['error'] = "User name and password are required";
        header("Location: login.php");
        return;
    }
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Log in</title>
    <style media="screen">
      body {
        font-family: Arial;
      }
      #msg {
        color: red;
      }
    </style>
  </head>
  <body>

    <h1>Please Log In</h1>

    <?php
      if (isset($_SESSION['error'])) {
          echo("<p id='msg'>".$_SESSION['error']."</p>");
          unset($_SESSION['error']);
      }
    ?>

    <form method="post">
        <p>Email: <input type="text" name="name"></p>
        <p>Password: <input type="password" name="pwd"></p>
        <input type="submit" value="Log in">
        <input type="button" value="Clear" onclick="location.href='login.php'; return false;">
    </form>

  </body>
</html>
